import Vue from 'vue'
import router from '../router'

export default {
  user: {
    get authenticated(){  return Vue.cookie.get('access_token')!=null;}
  },
  logout(){
    Vue.cookie.remove('access_token');
    this.user.authenticated='';
    //router.push({'name':'Login'});
  },
  login(username, password){
    console.log('username '+username);
    let params = new URLSearchParams();
    params.append('username',username);
    params.append('password',password);
    params.append('grant_type','password');
    params.append('client_id','csc-ui');
    Vue.axios.post('https://stopalien.com:8086/uaa/oauth/token', params,
                  {headers: {'Authorization':'Basic '+btoa("csc-ui:secret")}}).then((response)=>{
      this.setToken(response.data);
      this.user.authenticated='';
    }, (error)=>{
      console.log('error in authenticating');
      console.log(error);
    });
  },
  setToken(token){
    let expireDays = (1*token.expires_in) / 60 / 60 / 24;
    Vue.cookie.set('access_token',token.access_token, expireDays);
    //router.push({'path':'csc'})
  },
  checkAuth(config){
    console.log('checking auth')
    console.log('loggedin'+this.user.authenticated)
    if(!config.url.includes('stopalien.com:8086') && config.url.includes('stopalien.com') && this.user.authenticated){
      config.headers['Authorization']='Bearer '+Vue.cookie.get('access_token');
    }
  }
}
