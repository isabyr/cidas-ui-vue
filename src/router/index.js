import Vue from 'vue'
import Router from 'vue-router'
import KAMAZ from '@/components/KAMAZ'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path:'/kamaz',
      name:'kamaz',
      component: KAMAZ
    }
  ]
})
