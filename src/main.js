// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueJsCookie from 'vue-js-cookie'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueDragDrop from 'vue-drag-drop'
import VueLodash from 'vue-lodash'
import auth from './auth'

import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
Vue.component('icon', Icon)

import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCvpo1JTRSI2haq-Mb2uQHCjsY6k7hk7c4',
    libraries: 'drawing'
    // libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)

    //// If you want to set the version, you can do so:
    // v: '3.26',
  },

  //// If you intend to programmatically custom event listener code
  //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
  //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
  //// you might need to turn this on.
  // autobindAllEvents: false,

  //// If you want to manually install components, e.g.
  //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  //// Vue.component('GmapMarker', GmapMarker)
  //// then disable the following:
  // installComponents: true,
});

import GmapCluster from 'vue2-google-maps/dist/components/cluster' // replace src with dist if you have Babel issues
Vue.component('GmapCluster', GmapCluster);

import GmapPolyline from 'vue2-google-maps/dist/components/polyline' // replace src with dist if you have Babel issues
Vue.component('GmapPolyline', GmapPolyline);

Vue.use(VueAxios, axios)
Vue.use(BootstrapVue);
Vue.use(VueJsCookie);
Vue.use(VueDragDrop);

const options = { name: 'lodash' } // customize the way you want to call it

Vue.use(VueLodash, options) // options is optional



// import GmapCluster from 'vue2-google-maps/src/components/cluster'
// Vue.component('GmapCluster', GmapCluster)


Vue.config.productionTip = false;

Vue.axios.interceptors.request.use(function(config){
  auth.checkAuth(config);
  return config;
}, function (error) {
  // Do something with request error
  return Promise.reject(error);
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
