## Requirements
- Node.js
- npm
- Web browser(Firefox is recommended)

## Running and Building

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# copy files to web server
cp -r dist/* /var/www/kamaz/
```

## Components
[KAMAZ](https://gitlab.com/isabyr/cidas-ui-vue/blob/master/src/components/KAMAZ.vue) is the only component responsible for connection and visualization.

## After running `npm run dev`
Go to http://localhost:8080/#/kamaz

There you should define the ROS server ip address and establish connection. 
UI client transforms RTK coordinates received from the ROS server based on origin: `{lat: 51.096212, lng: 71.396347}`.
You should redefine this variable based on your setup.
